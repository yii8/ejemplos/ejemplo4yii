<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $Codigo
 * @property string $Nombre
 * @property string $Apellido1
 * @property string $Apellido2
 * @property string $Direccion
 * @property string $Poblacion
 * @property string $Fechanacimiento
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellido1', 'Apellido2', 'Direccion', 'Poblacion', 'Fechanacimiento'], 'required'],
            [['Fechanacimiento'], 'safe'],
            [['Nombre', 'Apellido1', 'Apellido2', 'Poblacion'], 'string', 'max' => 100],
            [['Direccion'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Codigo' => 'Codigo',
            'Nombre' => 'Nombre',
            'Apellido1' => 'Primer Apellido',
            'Apellido2' => 'Segundo Apellido',
            'Direccion' => 'Direccion',
            'Poblacion' => 'Poblacion',
            'Fechanacimiento' => 'Fecha de nacimiento',
        ];
    }
}
